<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private const RAND_ID_LOOP = 20;
    private const ALGO_KEY = "1234567890abcdefghijklmnopqrstyuvwxyzABCDEFGHIJKLMNOPQRSTYUVWXYZ";
    protected const SECLOOP = 3;
    protected const DECRYPT_KEY = "=";
    public const PREF = "ES";


    public static function randId($toRand): string
    {
        $filled = "";
        $check = "";
        $len = strlen(self::ALGO_KEY) - 1;

        for ($i = 0; $i < self::RAND_ID_LOOP; $i++) {
            $filled .= self::ALGO_KEY[rand(0, $len)];
        }

        switch ($toRand) {
            case "History":
                $check = History::all("id");
                break;

            default:
                $check = User::all("id");
                break;
        }

        foreach ($check as $chk) {
            if ($filled === $chk->id) {
                static::randId($toRand);
            }
        }
        return $filled;
    }
    public static function writeHist(...$data)
    {
        History::create([
            "id" => static::randId("History"),
            "from" => $data[0],
            "status" => $data[1],
            "to" => $data[2],
            "amount" => $data[3],
            "tax" => $data[4]
        ]);
    }
}
