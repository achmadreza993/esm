<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    public function index(Request $request)
    {
        $till = date("Y-m-d");
        if($request->input("day") == 'hariIni'){
            $akun = History::whereDate("created_at", date("Y-m-d"));
        }else if($request->input("day") == 'all'){
            $akun = new History();
        }else if($request->input("day") == 'custom'){
            if($request->input("range")){
                $akun = History::whereDate("created_at",">=",$request->input("from"))->whereDate("created_at","<=",$request->input("to"));
            }else{
                $akun = History::whereDate("created_at", $request->input("from"));
            }
        }else{
            $date_predict = date("Y-m-d", time() - ($request->input("day") * 86400));
            $akun = History::whereDate("created_at",">=",$date_predict)->whereDate("created_at","<=",$till);
        }
        $akun = $akun->orderBy("created_at", "DESC")->get();

        foreach ($akun as $a) {
            $a->from = User::select('name')->where('esm_number', $a->from)->first()->name;
            $a->to = User::select("name")->where("esm_number", $a->to)->first()->name;
        }
        return $akun;
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\History  $history
     * @return \Illuminate\Http\Response
     */

    public function show(History $history)
    {
        $history->from = User::select('name','username','esm_number')->where('esm_number', $history->from)->first();
        $history->to = User::select('name','username','esm_number')->where('esm_number', $history->to)->first();
        return $history;
    }
    public function transfer(Request $request, User $user1, $esm)
    {
        $balance = $request->input("balance");
        $status = $request->input("status");
        $tax = $request->input("tax");

        DB::beginTransaction();
        try {
            $user1->update([
                "balance" => $user1->balance - ($balance + $tax)
            ]);

            $user2 = User::where("esm_number", $esm)->get()->first();
            $user2->update([
                "balance" => $user2->balance + $balance
            ]);

            static::writeHist($user1->esm_number, $status, $esm, $balance, $tax);
            DB::commit();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return response()->json(["status"=>false,"message"=> $e]);
        }

        return response("success");
    }
    public function charge(Request $request)
    {
        $fixedAccNum = static::normalizeAccNumber($request->input("accNum"));
        return User::where("id", $fixedAccNum)->get()->first();
    }
    private static function normalizeAccNumber($toFix)
    {
        return self::PREF . substr($toFix, 2);
    }

    public function getChart(Request $request){
        $final = [];
        $res = [];
        $options = (object)["date"=>"Y-m-d", "substr"=>10, "loop"=>86400, "from"=>"", "to"=>""];

        if($request->input("day") == 'all'){
            $options->from = substr(History::select("created_at")->get()->min("created_at"),0,4);
            $options->to = substr(History::select("created_at")->get()->max("created_at"),0,4);
            $options->substr =  4;
            $res= History::select('tax',"created_at")->orderBy("created_at", "ASC")->get();
            if(count($res) != 0){
                for ($a = $options->from; $a <= $options->to;  $a++) {
                    $final[count($final)] = (object)["tax"=>0, "date"=>''.$a];
                }
            }
        }else {
            if($request->input("day") == 'custom'){
                $options->from = date("Y-m-d",strtotime($request->input("from"))-86400);
                if($request->input("range")){
                    $options->to = $request->input("to");
                }else{
                    $options->to = $request->input("from");
                }
            }else{
                $options->from = date("Y-m-d",time() - ($request->input("day") * 86400));
                $options->to = date("Y-m-d");
                if($request->input("day") > 30){
                    $options->loop = 2592000;
                    $options->date = "Y-m";
                    $options->substr = 7;
                }
            }

            $res= History::select('tax',"created_at")->whereDate("created_at",">=",$options->from)->whereDate("created_at","<=", $options->to)->get();
            for ($loopDate = strtotime($options->from); $loopDate <= strtotime($options->to);  $loopDate+=$options->loop) {
                $final[count($final)] = (object)["tax"=>0, "date"=>date($options->date,$loopDate)];
            }
        }

        if(count($res) != 0){
            foreach($res as $s){
                $s->date = substr($s->created_at,0,$options->substr);
                unset($s->created_at);
            };
            foreach ($final as $f) {
                foreach ($res as $r) {
                    if($f->date == $r->date){
                        $f->tax += intval($r->tax);
                    }
                }
            }
        }

        $resDate = [];
        $resTax = [];
        foreach ($final as $f) {
            array_push($resDate,$f->date);
            array_push($resTax,$f->tax);
        }
        return ["tax"=>$resTax,"date"=>$resDate];
    }
}
