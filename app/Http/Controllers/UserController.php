<?php

namespace App\Http\Controllers;

use App\Models\History;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\Response;


class UserController extends Controller
{
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $faker = Factory::create('ar_SA');

    $code = $request->input('secret');
    $enc = $request->query("enc");

    $exp_arr = $this->reshuff($enc);

    $email = $exp_arr[0];
    $name = $exp_arr[1];
    $telp = $exp_arr[2];
    $rek = $exp_arr[3];

    $verified = $this->verifyIncomingCode($code, $enc);
    if (!$verified) {
      return response(["message" => "Who are you!? just go away or i will chase you!!"]);
    }

    DB::beginTransaction();
    try {

      User::create([
        'id' => static::randId("User"),
        'secretCode' => $this->generateSecretCode()
      ]);

      User::where('id', $verified)->update([
        'id' => static::randId("User"),
        'username' => $request->input('username'),
        'esm_number' => 'ESM' . $rek,
        'name' => $name,
        'phone' => $telp,
        'image' => "default.png",
        'email' => $email,
        'password' => password_hash($request->input('password'), PASSWORD_DEFAULT),
        'secretCode' => null,
        'verified' => '1'
      ]);
      DB::commit();
    } catch (ModelNotFoundException $e) {
      DB::rollBack();
      return response('content', Response::HTTP_BAD_REQUEST);
    }
    return response('success', Response::HTTP_OK);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request)
  {
      if($request->input("status") == "esmNumber"){
        return User::where("esm_number", $request->input("data"))->first();
      }
      if($request->input("status") == "username"){
        return User::select("id", "esm_number", "username", "name", "balance", "image")->where("username", "like", "%".$request->input("data")."%")->limit(10)->get();
      }
  }

  public function check(Request $request)
  {
      $toCheck = "";
      $status =$request->input('status');
      if ($status == 'username') {
          $toCheck = $request->input('username');
          if(!User::where("username",$toCheck)->first()){
              return response(["check"=>true]);
            };
            return response(["check"=>false]);
        }else if ($status == 'accNum') {
            $toCheck = $request->input('accNum');
            $normAcc = "ESM".substr($toCheck,2);
            if(User::where("esm_number",$normAcc)->first()){
                return response(["checkESM"=>false]);
            };
            $h = Http::post("https://schoolbank.smkn1boyolangu.sch.id/api/resend/activation",["accNum" => $toCheck]);
            return response($h->json());
        }


    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $username = $request->input('username');
    if ($request->input('status') == 'username') {
        $user->update(['username' => $username]);
      return response(['status' => "success"]);
    }

    $check = password_verify($request->input("password"), $user->password);

    if (!$check) {
      return response(['status' => 'failed']);
    }
    if ($request->input('status') == 'personal') {
      $user->update(['email' => $request->input('email'), 'phone' => $request->input('phone')]);
    }
    if ($request->input('status') == 'password') {
      $new = Hash::make($request->input("new"));
      $user->update(['password' => $new]);
    }
    return response(['status' => 'success']);
  }

  public function getHist(User $user)
  {
    $hist = History::where("from", $user->esm_number)->orWhere("to", $user->esm_number)->orderBy('created_at', 'desc')->get();
    foreach($hist as $h){
        if($h->status == 'pay' || $h->status == 'transfer'){
            $h->from = User::select('username')->where('esm_number',$h->from)->first()->username;
            $h->to = User::select('username')->where('esm_number',$h->to)->first()->username;
        }
    }
    return response()->json($hist);
  }
  private function verifyIncomingCode(...$credentials)
  {
    if (empty($credentials[0]) || empty($credentials[1])) {
      return false;
    }

    $user = User::where('verified', '0')->get(["id","secretCode"]);


    foreach ($user as $u) {
      if (sha1($u->secretCode) === $credentials[0]) {
        return $u->id;
      }
    }
    return false;
  }

  public function secretHashed(): string
  {
    return sha1($this->getSecretCode());
  }

  private function getSecretCode(): string
  {
    $user = User::where('verified', '0')->get()->first();
    return $user->secretCode;
  }

  private function generateSecretCode(): string
  {
    $faker = Factory::create();

    $secretCode = "";

    for ($i = 0; $i < self::SECLOOP; $i++) {
      $secretCode .= $faker->randomAscii;
    }

    return $secretCode;
  }


  private function decryptToRegist($crypts): string
  {
        return Crypt::decryptString($crypts["first"] . $crypts["second"]);
  }

  private function reshuff($enc): array
  {

    $pmt = floor(strlen($enc) / 2);
    $crypts = [
      "first" => substr($enc, $pmt),
      "second" => substr($enc, 0, $pmt),
    ];

    $decrypted = $this->decryptToRegist($crypts);
    return explode(",", $decrypted);
  }
  public function userCharge(Request $request)
  {
      $balance = $request->input("balance");
      $esm = $request->input("bankNumber");
      $tax = $request->input("tax");

      $user = User::where("esm_number", $request->input("bankNumber"))->get()->first();
      static::writeHist($user->esm_number, "charge", $esm, $balance, $tax);
      return $user->update(["balance" => $user->balance +  $balance]);
  }
}

