<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use HasFactory;

    protected $keyType = "string";

    protected $fillable = [
        "from",
        "user_id",
        "status",
        "id",
        "amount",
        "tax",
        "to"
    ];
}
