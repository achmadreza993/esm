<?php

namespace Database\Factories;

use App\Http\Controllers\Controller;
use App\Models\History;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = History::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => Controller::randId("History"),
            'from' => $this->faker->bankAccountNumber,
            'to' => $this->faker->bankAccountNumber,
            'status' => $this->faker->randomElement(['pay','charge','transfer']),
            'amount' => $this->faker->randomDigit+10000,
            'tax' => $this->faker->randomDigit+100,
        ];
    }
}
