require("./bootstrap");

import Vue from "vue";
import Croppa from "vue-croppa";
import router from "./routes";
import store from "./vuex/index";
import "vue-croppa/dist/vue-croppa.css";


Vue.component("mynav", require("./components/Nav.vue").default);
Vue.use(Croppa);

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        axios
            .get("/api/auth")
            .then(() => {
                next();
            })
            .catch(() => {
                return next({
                    path: "/login"
                });
            });
    } else if (to.matched.some(record => record.meta.requiresGuest)) {
        axios
            .get("/api/auth")
            .then(res => {
                return next({
                    path: "/"
                });
            })
            .catch(() => {
                next();
            });
    } else if (to.matched.some(record => record.meta.requiresAdmin)) {
        axios
            .get("/api/user")
            .then(res => {
                if (res.data.role !== "admin") {
                    return next({
                        path: "/"
                    });
                } else {
                    next();
                }
            })
            .catch(() => {
                return next({
                    path: "/login"
                });
            });
    } else {
        next(); // make sure to always call next()!
    }
});

const app = new Vue({
    el: "#app",
    router,
    store
});
