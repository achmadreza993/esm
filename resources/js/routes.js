import Vue from "vue";
import router from "vue-router";
Vue.use(router);

import Home from "./components/Home.vue";
import Login from "./components/Login.vue";
import Activate from "./components/Activate.vue";
import Registration from "./components/Registration.vue";
import Transfer from "./components/Transfer.vue";
import Pay from "./components/Pay.vue";
import PayCode from "./components/PayCode.vue";
import Profile from "./components/Profile.vue";
import TransactionHistory from "./components/TransactionHistory.vue";
import TransactionDetail from "./components/TransactionDetail.vue";
import Setting from "./components/Setting.vue";
import NotFound from "./components/NotFound.vue";
import PhotoEditor from "./components/PhotoEditor.vue";

export default new router({
    mode: "history",
    routes: [
        {
            path: "/photoEditor",
            component: PhotoEditor,
            name: "photoEditor"
        },
        {
            path: "/",
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            name: "login",
            path: "/login",
            component: Login,
            meta: { requiresGuest: true }
        },
        {
            name: "activate",
            path: "/activate",
            component: Activate,
            meta: { requiresGuest: true }
        },
        {
            name: "registration",
            path: "/registration",
            component: Registration,
            meta: { requiresGuest: true }
        },
        {
            path: "/transfer",
            component: Transfer,
            meta: { requiresAuth: true }
        },
        {
            path: "/pay",
            component: Pay,
            meta: { requiresAuth: true }
        },
        {
            path: "/paycode",
            component: PayCode,
            meta: { requiresAuth: true }
        },
        {
            path: "/profile",
            component: Profile,
            meta: { requiresAuth: true }
        },
        {
            path: "/transactionHistory",
            component: TransactionHistory,
            meta: { requiresAuth: true }
        },
        {
            name: "transaction",
            path: "/transaction/:id",
            component: TransactionDetail,
            meta: { requiresAuth: true }
        },
        {
            path: "/setting",
            component: Setting,
            meta: { requiresAuth: true }
        },
        {
            name: "notFound",
            path: "*",
            component: NotFound
        }
    ]
});
