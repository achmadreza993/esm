export default {
    namespaced: true,
    state: {
        history: [],
        user: {},
        schoolbank: 0,
        payTarget: {
            username: null
        },
        resQuery: [],
        payStatus: null,
        transactionDetail: {}
    },
    actions: {
        getUser({ state, dispatch }, all = true) {
            axios
                .get("/api/currentUser")
                .then(res => {
                    state.user = res.data;
                    setTimeout(() => dispatch("getSchoolbank"), 1000);
                    if (all) {
                        dispatch("getHistory");
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        },
        getSchoolbank({ state }) {
            axios
                .get(
                    `https://schoolbank.smkn1boyolangu.sch.id/api/myBalance/${state.user.esm_number}`
                )
                .then(res => {
                    state.schoolbank = res.data.myBalance;
                })
                .catch(err => {
                    console.log(err);
                });
        },
        getHistory({ state }) {
            axios
                .get(`/api/history/${state.user.id}`)
                .then(result => {
                    state.history = result.data;
                })
                .catch(err => {
                    console.log(err);
                });
        },
        getPayTarget({ state }, data) {
            axios
                .post(`/api/userShow`, { data: data.data, status: data.status })
                .then(res => {
                    if (data.status == "username") {
                        if (res.data.length == 0) {
                            state.resQuery = [];
                        } else {
                            state.resQuery = res.data;
                        }
                    }
                    if (data.status == "esmNumber") {
                        if (res.data === "") {
                            state.payTarget = {
                                username: null
                            };
                        } else {
                            state.payTarget = res.data;
                        }
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        },
        pay({ state }, data) {
            axios
                .post(`/api/transfer/${state.user.id}/${data.target}`, {
                    balance: data.amount,
                    status: data.status,
                    tax: data.tax
                })
                .then(res => {
                    state.payStatus = res.data;
                })
                .catch(err => {
                    console.log(err);
                });
        },
        getTransaction({ state }, id) {
            axios
                .get(`/api/transaction/${id}`)
                .then(res => {
                    state.transactionDetail = res.data;
                })
                .catch(err => {
                    console.log(err);
                });
        },
        charge({ state, dispatch }, amount) {
            axios
                .post("https://schoolbank.smkn1boyolangu.sch.id/api/charge", {
                    amount: amount,
                    bankNumber: state.user.esm_number,
                    tax: parseInt(amount) * 0.02
                })
                .then(() => {
                    dispatch("getUser");
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }
};
