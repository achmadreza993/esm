import Axios from "axios";

export default {
    namespaced: true,
    state: {
        notif: {
            State: false,
            status: null
        }
    },
    mutations: {
        clearNotif(state) {
            state.notif = {
                State: false,
                status: null
            };
        }
    },
    actions: {
        update({ state }, data) {
            Axios.put(`/api/user/${data.id}`, data)
                .then(res => {
                    if (res.data.status == "success") {
                        state.notif.State = true;
                        state.notif.status = true;
                    }
                    if (res.data.status == "failed") {
                        state.notif.State = true;
                        state.notif.status = false;
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }
};
