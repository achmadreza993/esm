export default {
    namespaced: true,
    state: {
        user: {},
        status: null,
        secret: null,
        activationMsg: "",
        usernameStatus: true
    },
    mutations: {
        setUser(state, data) {
            state.user = data;
        },
        clearStatus(state) {
            state.status = null;
        },
        clearMsg(state) {
            state.activationMsg = "";
        }
    },
    actions: {
        getUser({ commit }) {
            axios.get("/api/currentUser").then(res => {
                if (res.data) {
                    commit("setUser", res.data);
                }
            });
        },

        loginUser({ state, commit }, data) {
            commit("clearError");
            axios.get("/sanctum/csrf-cookie").then(res => {
                axios
                    .post("/login", data)
                    .then(() => {
                        state.status = true;
                        window.location.replace("/");
                    })
                    .catch(err => {
                        if (err.response.data) {
                            let Err = err.response.data.errors;
                            if (Err.email != null && Err.password == null) {
                                state.status = Err.email[0];
                            } else if (
                                Err.email == null &&
                                Err.password != null
                            ) {
                                state.status = Err.password[0];
                            } else if (
                                Err.email != null &&
                                Err.password != null
                            ) {
                                state.status = "Email & Password was Wrong !!!";
                            }
                        }
                    });
            });
        },

        logoutUser({ state }) {
            axios.post("/logout").then(res => {
                state.user = {};
                window.location.replace("login");
            });
        },
        getSecret({ state }) {
            axios.get("/api/secret").then(res => {
                state.secret = res.data;
            });
        },
        Registration({ state }, data) {
            axios
                .post(`/api/user?enc=${data.enc}`, {
                    username: data.username,
                    password: data.password,
                    secret: state.secret
                })
                .then(() => {
                    window.location.replace("/login");
                });
        },
        activation({ state }, noRek) {
            axios
                .post("/api/check", { accNum: noRek, status: "accNum" })
                .then(res => {
                    if (!res.data.checkESM && res.data.checkESM != null) {
                        state.activationMsg =
                            "Your ESM Account has been activated";
                    }
                    if (!res.data.checkSB && res.data.checkSB != null) {
                        state.activationMsg =
                            "Your SchoolBank account number isn't registered";
                    }
                    if (res.data.checkSB && res.data.checkSB != null) {
                        state.activationMsg =
                            "Your activation email has been sent. Please check your inbox and spam";
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        },
        checkUsername({ state }, username) {
            axios
                .post("/api/check", { username: username, status: "username" })
                .then(res => {
                    state.usernameStatus = res.data.check;
                });
        }
    },
    getters: {
        getCurrentUser: state => {
            return state.user;
        }
    }
};
