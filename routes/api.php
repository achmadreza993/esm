<?php

use App\Http\Controllers\HistoryController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function () {
  Route::get('/currentUser', function (Request $request) {
    return $request->user();
  });
  Route::get('/auth', function () {
    return true;
  });
  Route::put('user/{user}', [UserController::class, "update"]);
});
Route::get('history/{user}', [UserController::class, 'getHist']);
Route::post('userShow', [UserController::class, "show"]);
Route::post('transfer/{user1}/{esm}', [HistoryController::class, 'transfer']);
Route::get('transaction/{history}', [HistoryController::class, 'show']);
Route::post('history', [HistoryController::class, 'index']);
Route::post('getChart', [HistoryController::class, 'getChart']);
  Route::get('secret', [UserController::class, 'secretHashed']);
  Route::post('user', [UserController::class, "store"]);
  Route::post('charge', [UserController::class, "userCharge"]);
  Route::post('check', [UserController::class, "check"]);
Route::post('upload/images/{user}', function (Request $request,User $user) {
    $filename = $request->file("file")->getClientOriginalName();
    $ext = $request->file("file")->getClientOriginalExtension();
    $filenameToStore = $filename.'_'.time().'.jpeg';
    $path = $request->file('file')->storeAs("public/avatars",$filenameToStore);
    $user->image = $filenameToStore;
    $user->save();
    return response("success");
});
